package net.pl3x.bukkit.pl3xsigns.nms.v1_9_R2;

import net.minecraft.server.v1_9_R2.BlockPosition;
import net.minecraft.server.v1_9_R2.ChatComponentText;
import net.minecraft.server.v1_9_R2.EntityPlayer;
import net.minecraft.server.v1_9_R2.IChatBaseComponent;
import net.minecraft.server.v1_9_R2.NBTTagCompound;
import net.minecraft.server.v1_9_R2.PacketPlayOutOpenSignEditor;
import net.minecraft.server.v1_9_R2.PacketPlayOutTileEntityData;
import net.minecraft.server.v1_9_R2.TileEntitySign;
import net.pl3x.bukkit.pl3xsigns.api.Packet;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_9_R2.block.CraftSign;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class PacketHandler implements Packet {
    @Override
    public void sendSignWindowPacket(Player player, Sign sign) {
        // make sign editable
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        TileEntitySign signTileEntity = ((CraftSign) sign).getTileEntity();
        signTileEntity.isEditable = true;
        try {
            Field h = signTileEntity.getClass().getDeclaredField("h");
            h.setAccessible(true);
            h.set(signTileEntity, entityPlayer);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        // send update packet
        BlockPosition position = new BlockPosition(sign.getX(), sign.getY(), sign.getZ());
        NBTTagCompound nbt = new NBTTagCompound();
        for (int i = 0; i < 4; i++) {
            String line = ChatColor.stripColor(sign.getLine(i).replace("\u00a7", "&"));
            line = IChatBaseComponent.ChatSerializer.a(new ChatComponentText(line));
            nbt.setString("Text" + (i + 1), line);
        }
        nbt.setString("text", "");
        nbt.setString("id", "Sign");
        nbt.setInt("x", sign.getX());
        nbt.setInt("y", sign.getY());
        nbt.setInt("z", sign.getZ());
        PacketPlayOutTileEntityData tileEntityPacket = new PacketPlayOutTileEntityData(position, 9, nbt);
        entityPlayer.playerConnection.sendPacket(tileEntityPacket);

        // send open sign packet
        PacketPlayOutOpenSignEditor signEditorPacket = new PacketPlayOutOpenSignEditor(position);
        entityPlayer.playerConnection.sendPacket(signEditorPacket);
    }
}
