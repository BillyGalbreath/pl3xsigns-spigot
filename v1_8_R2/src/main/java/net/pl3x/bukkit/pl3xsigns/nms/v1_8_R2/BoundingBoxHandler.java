package net.pl3x.bukkit.pl3xsigns.nms.v1_8_R2;

import net.minecraft.server.v1_8_R2.BlockPosition;
import net.minecraft.server.v1_8_R2.WorldServer;
import net.pl3x.bukkit.pl3xsigns.api.BoundingBox;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R2.CraftWorld;
import org.bukkit.util.Vector;

public class BoundingBoxHandler implements BoundingBox {
    private final Vector min;
    private final Vector max;

    BoundingBoxHandler(Block block) {
        BlockPosition pos = new BlockPosition(block.getX(), block.getY(), block.getZ());
        WorldServer world = ((CraftWorld) block.getWorld()).getHandle();
        net.minecraft.server.v1_8_R2.Block nmsBlock = world.getType(pos).getBlock();
        nmsBlock.updateShape(world, pos);
        min = new Vector(
                (double) pos.getX() + nmsBlock.B(),
                (double) pos.getY() + nmsBlock.D(),
                (double) pos.getZ() + nmsBlock.F()
        );
        max = new Vector(
                (double) pos.getX() + nmsBlock.C(),
                (double) pos.getY() + nmsBlock.E(),
                (double) pos.getZ() + nmsBlock.G()
        );
    }

    public Vector getMin() {
        return min;
    }

    public Vector getMax() {
        return max;
    }
}
