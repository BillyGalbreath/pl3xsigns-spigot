package net.pl3x.bukkit.pl3xsigns.nms.v1_9_R1;

import net.minecraft.server.v1_9_R1.BlockPosition;
import net.minecraft.server.v1_9_R1.ChatComponentText;
import net.minecraft.server.v1_9_R1.EntityPlayer;
import net.minecraft.server.v1_9_R1.IChatBaseComponent;
import net.minecraft.server.v1_9_R1.PacketPlayOutOpenSignEditor;
import net.minecraft.server.v1_9_R1.PacketPlayOutUpdateSign;
import net.minecraft.server.v1_9_R1.TileEntitySign;
import net.pl3x.bukkit.pl3xsigns.api.Packet;
import org.bukkit.ChatColor;
import org.bukkit.block.Sign;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R1.block.CraftSign;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class PacketHandler implements Packet {
    @Override
    public void sendSignWindowPacket(Player player, Sign sign) {
        // make sign editable
        EntityPlayer entityPlayer = ((CraftPlayer) player).getHandle();
        TileEntitySign signTileEntity = ((CraftSign) sign).getTileEntity();
        signTileEntity.isEditable = true;
        try {
            Field h = signTileEntity.getClass().getDeclaredField("h");
            h.setAccessible(true);
            h.set(signTileEntity, entityPlayer);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        // send update packet
        BlockPosition position = new BlockPosition(sign.getX(), sign.getY(), sign.getZ());
        IChatBaseComponent[] components = new IChatBaseComponent[4];
        for (int i = 0; i < 4; i++) {
            String line = ChatColor.stripColor(sign.getLine(i).replace("\u00a7", "&"));
            components[i] = new ChatComponentText(line);
        }
        PacketPlayOutUpdateSign updateSignPacket = new PacketPlayOutUpdateSign(((CraftWorld) sign.getWorld()).getHandle().b(), position, components);
        entityPlayer.playerConnection.sendPacket(updateSignPacket);

        // send open sign packet
        PacketPlayOutOpenSignEditor signEditorPacket = new PacketPlayOutOpenSignEditor(position);
        entityPlayer.playerConnection.sendPacket(signEditorPacket);
    }
}
