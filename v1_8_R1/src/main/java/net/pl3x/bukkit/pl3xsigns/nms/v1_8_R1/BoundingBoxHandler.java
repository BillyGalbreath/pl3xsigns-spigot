package net.pl3x.bukkit.pl3xsigns.nms.v1_8_R1;

import net.minecraft.server.v1_8_R1.BlockPosition;
import net.minecraft.server.v1_8_R1.WorldServer;
import net.pl3x.bukkit.pl3xsigns.api.BoundingBox;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R1.CraftWorld;
import org.bukkit.util.Vector;

public class BoundingBoxHandler implements BoundingBox {
    private final Vector min;
    private final Vector max;

    BoundingBoxHandler(Block block) {
        BlockPosition pos = new BlockPosition(block.getX(), block.getY(), block.getZ());
        WorldServer world = ((CraftWorld) block.getWorld()).getHandle();
        net.minecraft.server.v1_8_R1.Block nmsBlock = world.getType(pos).getBlock();
        nmsBlock.updateShape(world, pos);
        min = new Vector(
                (double) pos.getX() + nmsBlock.z(),
                (double) pos.getY() + nmsBlock.B(),
                (double) pos.getZ() + nmsBlock.D()
        );
        max = new Vector(
                (double) pos.getX() + nmsBlock.A(),
                (double) pos.getY() + nmsBlock.C(),
                (double) pos.getZ() + nmsBlock.E()
        );
    }

    public Vector getMin() {
        return min;
    }

    public Vector getMax() {
        return max;
    }
}
