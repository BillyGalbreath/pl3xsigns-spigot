package net.pl3x.bukkit.pl3xsigns.nms.v1_10_R1;

import io.netty.channel.Channel;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_10_R1.BlockPosition;
import net.minecraft.server.v1_10_R1.ChatComponentText;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.NBTTagCompound;
import net.pl3x.bukkit.pl3xsigns.api.SignData;
import net.pl3x.bukkit.pl3xsigns.api.SignPacket;
import net.pl3x.bukkit.pl3xsigns.api.event.LoadSignEvent;
import net.pl3x.bukkit.pl3xsigns.api.event.UpdateSignEvent;
import net.pl3x.bukkit.pl3xsigns.nms.v1_10_R1.protocol.Reflection;
import net.pl3x.bukkit.pl3xsigns.nms.v1_10_R1.protocol.Reflection.FieldAccessor;
import net.pl3x.bukkit.pl3xsigns.nms.v1_10_R1.protocol.TinyProtocol;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class SignPacketHandler implements SignPacket {
    private Class<?> tileEntityClass = Reflection.getClass("{nms}.PacketPlayOutTileEntityData");
    private FieldAccessor<Integer> TILE_ENTITY_SIGN_INTEGER = Reflection.getField(tileEntityClass, int.class, 0);
    private FieldAccessor<BlockPosition> TILE_ENTITY_BLOCK_POSITION = Reflection.getField(tileEntityClass, BlockPosition.class, 0);
    private FieldAccessor<NBTTagCompound> TILE_ENTITY_NBT_COMPOUND = Reflection.getField(tileEntityClass, NBTTagCompound.class, 0);

    private Class<?> mapChunkClass = Reflection.getClass("{nms}.PacketPlayOutMapChunk");
    private FieldAccessor<List> MAP_CHUNK_NBT_LIST = Reflection.getField(mapChunkClass, List.class, 0);

    public SignPacketHandler(JavaPlugin plugin) {
        new TinyProtocol(plugin) {
            @Override
            public Object onPacketOutAsync(Player receiver, Channel channel, Object packet) {
                if (tileEntityClass.isInstance(packet)) {
                    if (TILE_ENTITY_SIGN_INTEGER.get(packet) != 9) {
                        return packet; // not a sign
                    }

                    BlockPosition pos = TILE_ENTITY_BLOCK_POSITION.get(packet);
                    NBTTagCompound nbt = TILE_ENTITY_NBT_COMPOUND.get(packet);

                    if (pos == null || nbt == null) {
                        return packet; // packet missing info; ignore it
                    }

                    if (nbt.hasKey("Pl3xSignsEditor") && nbt.getBoolean("Pl3xSignsEditor")) {
                        return packet; // this is a custom sign editor packet; ignore
                    }

                    Block block = receiver.getWorld().getBlockAt(pos.getX(), pos.getY(), pos.getZ());
                    if (!(block.getState() instanceof Sign)) {
                        return packet; // position is not a sign
                    }

                    UpdateSignEvent event = new UpdateSignEvent(receiver, getLines(nbt), block.getLocation());
                    Bukkit.getPluginManager().callEvent(event);

                    setLines(nbt, event.getLines(), event.isForceColor() || receiver.hasPermission("sign.color"));

                    TILE_ENTITY_NBT_COMPOUND.set(packet, nbt);
                } else if (mapChunkClass.isInstance(packet)) {
                    List<NBTTagCompound> nbtList = new ArrayList<>();
                    List rawList = MAP_CHUNK_NBT_LIST.get(packet);
                    for (Object obj : rawList) {
                        if (obj instanceof NBTTagCompound) {
                            NBTTagCompound nbt = (NBTTagCompound) obj;
                            if (!nbt.hasKey("id") || !"Sign".equals(nbt.getString("id"))) {
                                continue; // not a sign NBT
                            }
                            nbtList.add(nbt);
                        }
                    }

                    List<SignData> signDatas = new ArrayList<>();
                    for (NBTTagCompound nbt : nbtList) {
                        signDatas.add(new SignData(getLines(nbt), nbt.getInt("x"), nbt.getInt("y"), nbt.getInt("z")));
                    }

                    LoadSignEvent event = new LoadSignEvent(receiver, signDatas);
                    Bukkit.getPluginManager().callEvent(event);

                    signDatas = event.getSignDatas();
                    for (int i = 0; i < signDatas.size(); i++) {
                        NBTTagCompound nbt = nbtList.get(i);
                        setLines(nbt, signDatas.get(i).getLines(), event.isForceColor() || receiver.hasPermission("sign.color"));
                    }

                    MAP_CHUNK_NBT_LIST.set(packet, nbtList);
                }

                return packet;
            }
        };
    }

    private String getLine(String raw) {
        BaseComponent[] components = ComponentSerializer.parse(raw);
        String componentText = "";
        for (BaseComponent component : components) {
            String text = ((TextComponent) component).getText();
            if (text == null || text.isEmpty() && component.getExtra() != null) {
                for (BaseComponent extra : component.getExtra()) {
                    text = text + ((TextComponent) extra).getText();
                }
            }
            componentText = componentText + text;
        }
        return componentText;
    }

    private String[] getLines(NBTTagCompound nbt) {
        String[] lines = new String[4];
        for (int i = 0; i < 4; i++) {
            int j = i + 1;
            lines[i] = nbt.hasKey("Text" + j) ? getLine(nbt.getString("Text" + j)) : "";
        }
        return lines;
    }

    private void setLines(NBTTagCompound nbt, String[] lines, boolean colorize) {
        for (int i = 0; i < 4; i++) {
            int j = i + 1;
            String text = lines[i];
            if (colorize) {
                text = ChatColor.translateAlternateColorCodes('&', text);
            }
            text = IChatBaseComponent.ChatSerializer.a(new ChatComponentText(text));
            nbt.setString("Text" + j, text);
        }
    }
}
