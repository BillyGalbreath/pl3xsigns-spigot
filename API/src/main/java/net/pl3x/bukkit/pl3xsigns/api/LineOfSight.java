package net.pl3x.bukkit.pl3xsigns.api;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface LineOfSight {
    Block getTargetBlock(Player player, int distance, double accuracy);
}
