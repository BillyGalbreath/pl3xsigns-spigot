package net.pl3x.bukkit.pl3xsigns.api;

import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

public interface Packet {
    void sendSignWindowPacket(Player player, Sign sign);
}
