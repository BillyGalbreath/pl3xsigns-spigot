package net.pl3x.bukkit.pl3xsigns.api;

import org.bukkit.util.Vector;

public interface BoundingBox {
    Vector getMin();

    Vector getMax();
}
