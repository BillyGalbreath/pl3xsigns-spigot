package net.pl3x.bukkit.pl3xsigns.configuration;

import net.pl3x.bukkit.pl3xsigns.Logger;
import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    COMMAND_NO_PERMISSION("&4You do not have permission for that command!"),
    PLAYER_COMMAND("&4This command is only available to players."),

    CANNOT_PUT_SIGN_HERE("&4Cannot place sign here!"),
    CANNOT_EDIT_SIGN_HERE("&4Cannot edit sign here!"),
    NO_LINE_SPECIFIED("&4You must specify a line number to append to!"),
    NOT_LOOKING_AT_SIGN("&4Not looking at a sign!"),
    LINE_MUST_BE_NUMBER("&4Line must be a number!"),
    LINE_OUT_OF_RANGE("&4Acceptable line range is 1 - 4 only!"),
    CLIPBOARD_EMPTY("&4Clipboard is empty! Use /signcopy first."),
    NOTHING_TO_UNDO("&4No edits to undo!"),
    UNDO_SIGN_MISSING("&4Sign to undo is no longer there! Removed from undo history!"),
    SIGN_ALREADY_OPEN("&4Someone is already editing this sign!"),

    SIGN_APPENDED("&dSign appended."),
    SIGN_COPIED("&dSign copied. Use /signpaste to paste."),
    SIGN_EDITED("&dSign edited."),
    SIGN_PASTED("&dSign pasted."),
    SIGN_UNDONE("&dSing edit undone."),

    VERSION("&d{plugin} v{version}."),
    RELOAD("&d{plugin} v{version} reloaded."),
    PLUGIN_DISABLED("&4{plugin} is disabled. Please check console logs for more information.");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Pl3xSigns.getPlugin(Pl3xSigns.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Pl3xSigns.getPlugin(Pl3xSigns.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
