package net.pl3x.bukkit.pl3xsigns;

import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class Chat {
    private final String message;

    public Chat(Lang lang) {
        this(lang.toString());
    }

    public Chat(String message) {
        this.message = ChatColor.translateAlternateColorCodes('&', message);
    }

    public void send(CommandSender recipient) {
        if (message == null || ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
