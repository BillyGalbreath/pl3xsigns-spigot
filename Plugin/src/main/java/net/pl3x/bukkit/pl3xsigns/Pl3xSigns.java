package net.pl3x.bukkit.pl3xsigns;

import net.pl3x.bukkit.pl3xsigns.api.LineOfSight;
import net.pl3x.bukkit.pl3xsigns.api.Packet;
import net.pl3x.bukkit.pl3xsigns.api.SignPacket;
import net.pl3x.bukkit.pl3xsigns.command.CmdPl3xSigns;
import net.pl3x.bukkit.pl3xsigns.command.CmdSignAppend;
import net.pl3x.bukkit.pl3xsigns.command.CmdSignCopy;
import net.pl3x.bukkit.pl3xsigns.command.CmdSignEdit;
import net.pl3x.bukkit.pl3xsigns.command.CmdSignPaste;
import net.pl3x.bukkit.pl3xsigns.command.CmdSignUndo;
import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import net.pl3x.bukkit.pl3xsigns.listener.SignListener;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xSigns extends JavaPlugin {
    private Packet packetHandler;
    private LineOfSight lineOfSightHandler;
    private SignPacket signPacketHandler;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        if (!hasSupportedVersion()) {
            Logger.error("&4#&4#############################################################################&4#");
            Logger.error("&4#&c          Could not find support for this CraftBukkit version!               &4#");
            Logger.error("&4#&4                                                                             &4#");
            Logger.error("&4#&4     To prevent server crashes and other undesired behavior this plugin      &4#");
            Logger.error("&4#&4       is disabling itself from running. Please remove the plugin jar        &4#");
            Logger.error("&4#&4       from your plugins directory to free up the registered commands.       &4#");
            Logger.error("&4#&4#############################################################################&4#");
            return;
        }

        Bukkit.getPluginManager().registerEvents(new SignListener(this), this);

        getCommand("pl3xsigns").setExecutor(new CmdPl3xSigns(this));
        getCommand("signappend").setExecutor(new CmdSignAppend(this));
        getCommand("signcopy").setExecutor(new CmdSignCopy(this));
        getCommand("signedit").setExecutor(new CmdSignEdit(this));
        getCommand("signpaste").setExecutor(new CmdSignPaste(this));
        getCommand("signundo").setExecutor(new CmdSignUndo(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        Logger.info(getName() + " Disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        new Chat(Lang.PLUGIN_DISABLED
                .replace("{plugin}", getName()))
                .send(sender);
        return true;
    }

    private boolean hasSupportedVersion() {
        String packageName = this.getServer().getClass().getPackage().getName();
        String version = packageName.substring(packageName.lastIndexOf('.') + 1);

        try {
            final Class<?> packetClazz = Class.forName("net.pl3x.bukkit.pl3xsigns.nms." + version + ".PacketHandler");
            final Class<?> lineOfSightClazz = Class.forName("net.pl3x.bukkit.pl3xsigns.nms." + version + ".LineOfSightHandler");
            final Class<?> signPacketClazz = Class.forName("net.pl3x.bukkit.pl3xsigns.nms." + version + ".SignPacketHandler");
            if (Packet.class.isAssignableFrom(packetClazz)) {
                packetHandler = (Packet) packetClazz.getConstructor().newInstance();
                lineOfSightHandler = (LineOfSight) lineOfSightClazz.getConstructor().newInstance();
                signPacketHandler = (SignPacket) signPacketClazz.getConstructor(JavaPlugin.class).newInstance(this);
            }
        } catch (Exception e) {
            Logger.error(e.getLocalizedMessage());
            return false;
        }

        return true;
    }

    public Packet getPacketHandler() {
        return packetHandler;
    }

    public LineOfSight getLineOfSightHandler() {
        return lineOfSightHandler;
    }

    public SignPacket getSignPacketHandler() {
        return signPacketHandler;
    }
}
