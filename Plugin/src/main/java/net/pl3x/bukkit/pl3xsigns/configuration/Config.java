package net.pl3x.bukkit.pl3xsigns.configuration;

import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    MAX_UNDO(10),
    USE_OLD_LINE_OF_SIGHT(false),
    LINE_OF_SIGHT_DISTANCE(5),
    LINE_OF_SIGHT_ACCURACY(0.01);

    private final Pl3xSigns plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Pl3xSigns.getPlugin(Pl3xSigns.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public double getDouble() {
        return plugin.getConfig().getDouble(getKey(), (double) def);
    }

    public static void reload() {
        Pl3xSigns.getPlugin(Pl3xSigns.class).reloadConfig();
    }
}
