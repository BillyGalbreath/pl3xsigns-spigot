package net.pl3x.bukkit.pl3xsigns.listener;

import net.pl3x.bukkit.pl3xsigns.Chat;
import net.pl3x.bukkit.pl3xsigns.HistoryEntry;
import net.pl3x.bukkit.pl3xsigns.Logger;
import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;
import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import net.pl3x.bukkit.pl3xsigns.manager.HistoryManager;
import net.pl3x.bukkit.pl3xsigns.manager.SignManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class SignListener implements Listener {
    private final Pl3xSigns plugin;

    public SignListener(Pl3xSigns plugin) {
        this.plugin = plugin;
    }

    /*
     * Handle coloring/styling of signs
     */
    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onColorizeSign(SignChangeEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPermission("sign.color")) {
            return; // no color permission
        }

        Logger.debug("[SignChangeEvent] Player " + player.getName() + " has sign.color permission. Coloring text.");
        for (int i = 0; i < 4; i++) {
            event.setLine(i, ChatColor.translateAlternateColorCodes('&', event.getLine(i)));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onSignEditorClose(SignChangeEvent event) {
        Logger.debug("[SignChangeEvent] Event triggered.");

        if (SignManager.remove((Sign) event.getBlock().getState())) {
            Logger.debug("[SignChangeEvent] Removed sign editor.");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void saveSignEditHistory(SignChangeEvent event) {
        Logger.debug("[SignChangeEvent] Saving previous sign in undo history.");
        HistoryEntry history = new HistoryEntry((Sign) event.getBlock().getState());
        HistoryManager.getHistory(event.getPlayer()).add(history);
    }

    /*
     * Open sign editor window if sign was placed on another sign
     */
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public void signPlace(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return; // did not right click a block
        }

        Player player = event.getPlayer();
        if (player.isSneaking()) {
            return; // is sneaking. ignore.
        }
        if (!player.hasPermission("click.signedit")) {
            return; // no permission
        }

        Block block = event.getClickedBlock();
        if (!block.getType().equals(Material.WALL_SIGN) && !block.getType().equals(Material.SIGN_POST)) {
            return; // sign wasn't clicked
        }

        ItemStack mainHand;
        try {
            mainHand = player.getInventory().getItemInMainHand();
        } catch (NoSuchMethodError e) {
            // for v1.8.x
            //noinspection deprecation
            mainHand = player.getItemInHand();
        }
        if (!mainHand.getType().equals(Material.SIGN)) {
            return; // not holding a sign in main hand
        }

        Sign sign = (Sign) block.getState();
        if (SignManager.contains(sign)) {
            new Chat(Lang.SIGN_ALREADY_OPEN).send(player);
            event.setCancelled(true);
            return; // sign is already being edited by another user
        }

        Logger.debug("[PlayerInteractEvent]" + player.getName() + " opened sign editor.");
        plugin.getPacketHandler().sendSignWindowPacket(player, sign);

        SignManager.add(sign);
        event.setCancelled(true);
    }
}
