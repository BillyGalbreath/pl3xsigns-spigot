package net.pl3x.bukkit.pl3xsigns.command;

import net.pl3x.bukkit.pl3xsigns.Chat;
import net.pl3x.bukkit.pl3xsigns.Clipboard;
import net.pl3x.bukkit.pl3xsigns.Logger;
import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;
import net.pl3x.bukkit.pl3xsigns.configuration.Config;
import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import net.pl3x.bukkit.pl3xsigns.manager.ClipboardManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Set;

public class CmdSignCopy implements TabExecutor {
    private final Pl3xSigns plugin;

    public CmdSignCopy(Pl3xSigns plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }
        Player player = (Player) sender;
        if (!player.hasPermission("command.signcopy")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        Block block;
        if (Config.USE_OLD_LINE_OF_SIGHT.getBoolean()) {
            block = player.getTargetBlock((Set<Material>) null, 5);
        } else {
            block = plugin.getLineOfSightHandler().getTargetBlock(player,
                    Config.LINE_OF_SIGHT_DISTANCE.getInt(),
                    Config.LINE_OF_SIGHT_ACCURACY.getDouble());
        }

        if (block == null || !(block.getState() instanceof Sign)) {
            new Chat(Lang.NOT_LOOKING_AT_SIGN).send(sender);
            return true;
        }

        Sign sign = (Sign) block.getState();
        String[] lines = new String[4];
        for (int i = 0; i < 4; i++) {
            lines[i] = ChatColor.stripColor(sign.getLine(i).replace("\u00a7", "&"));
        }
        ClipboardManager.setClipboard(player, new Clipboard(lines));

        Logger.debug("[Copy Command] Player copied sign to clipboard.");
        new Chat(Lang.SIGN_COPIED).send(sender);
        return true;
    }
}
