package net.pl3x.bukkit.pl3xsigns.command;

import net.pl3x.bukkit.pl3xsigns.Chat;
import net.pl3x.bukkit.pl3xsigns.Clipboard;
import net.pl3x.bukkit.pl3xsigns.HistoryEntry;
import net.pl3x.bukkit.pl3xsigns.Logger;
import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;
import net.pl3x.bukkit.pl3xsigns.configuration.Config;
import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import net.pl3x.bukkit.pl3xsigns.manager.ClipboardManager;
import net.pl3x.bukkit.pl3xsigns.manager.HistoryManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

import java.util.List;
import java.util.Set;

public class CmdSignPaste implements TabExecutor {
    private final Pl3xSigns plugin;

    public CmdSignPaste(Pl3xSigns plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission("command.signpaste")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        Block block;
        if (Config.USE_OLD_LINE_OF_SIGHT.getBoolean()) {
            block = player.getTargetBlock((Set<Material>) null, 5);
        } else {
            block = plugin.getLineOfSightHandler().getTargetBlock(player,
                    Config.LINE_OF_SIGHT_DISTANCE.getInt(),
                    Config.LINE_OF_SIGHT_ACCURACY.getDouble());
        }

        if (block == null || !(block.getState() instanceof Sign)) {
            new Chat(Lang.NOT_LOOKING_AT_SIGN).send(sender);
            return true;
        }

        Clipboard clipboard = ClipboardManager.getClipboard(player);
        if (clipboard == null) {
            new Chat(Lang.CLIPBOARD_EMPTY).send(sender);
            return true;
        }

        String[] lines = clipboard.getLines();
        if (lines == null || lines.length == 0) {
            new Chat(Lang.CLIPBOARD_EMPTY).send(sender);
            return true;
        }
        HistoryEntry history = new HistoryEntry((Sign) block.getState()); // get current lines of sign

        Logger.debug("[Paste Command] Calling BlockBreakEvent.");
        BlockBreakEvent blockBreakEvent = new BlockBreakEvent(block, player);
        Bukkit.getServer().getPluginManager().callEvent(blockBreakEvent);
        if (blockBreakEvent.isCancelled()) {
            new Chat(Lang.CANNOT_PUT_SIGN_HERE).send(sender);
            return true;
        }

        Logger.debug("[Paste Command] Calling SignChangeEvent.");
        SignChangeEvent signChangeEvent = new SignChangeEvent(block, player, lines);
        Bukkit.getServer().getPluginManager().callEvent(signChangeEvent);
        if (signChangeEvent.isCancelled()) {
            new Chat(Lang.CANNOT_EDIT_SIGN_HERE).send(sender);
            return true;
        }

        lines = signChangeEvent.getLines();
        Sign sign = (Sign) block.getState();
        for (int i = 0; i < 4; i++) {
            sign.setLine(i, player.hasPermission("sign.color") ? ChatColor.translateAlternateColorCodes('&', lines[i]) : lines[i]);
        }
        sign.update();

        Logger.debug("[Paste Command] Sign text pasted.");
        HistoryManager.getHistory(player).add(history); // store original lines of sign
        new Chat(Lang.SIGN_PASTED).send(sender);
        return true;
    }
}
