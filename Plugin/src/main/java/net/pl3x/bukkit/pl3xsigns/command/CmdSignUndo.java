package net.pl3x.bukkit.pl3xsigns.command;

import net.pl3x.bukkit.pl3xsigns.Chat;
import net.pl3x.bukkit.pl3xsigns.History;
import net.pl3x.bukkit.pl3xsigns.HistoryEntry;
import net.pl3x.bukkit.pl3xsigns.Logger;
import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;
import net.pl3x.bukkit.pl3xsigns.configuration.Config;
import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import net.pl3x.bukkit.pl3xsigns.manager.HistoryManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

import java.util.List;
import java.util.Set;

public class CmdSignUndo implements TabExecutor {
    private final Pl3xSigns plugin;

    public CmdSignUndo(Pl3xSigns plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }

        Player player = (Player) sender;
        if (!player.hasPermission("command.signundo")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }

        History history = HistoryManager.getHistory(player);
        HistoryEntry lastEntry = history.get();
        if (lastEntry == null) {
            new Chat(Lang.NOTHING_TO_UNDO).send(sender);
            return true;
        }

        history.remove(); // remove from history, even if errors or not

        Block block;
        if (Config.USE_OLD_LINE_OF_SIGHT.getBoolean()) {
            block = player.getTargetBlock((Set<Material>) null, 5);
        } else {
            block = plugin.getLineOfSightHandler().getTargetBlock(player,
                    Config.LINE_OF_SIGHT_DISTANCE.getInt(),
                    Config.LINE_OF_SIGHT_ACCURACY.getDouble());
        }

        if (!(block.getState() instanceof Sign)) {
            new Chat(Lang.UNDO_SIGN_MISSING).send(sender);
            return true;
        }

        Logger.debug("[Undo Command] Calling BlockBreakEvent.");
        BlockBreakEvent blockBreakEvent = new BlockBreakEvent(block, player);
        Bukkit.getServer().getPluginManager().callEvent(blockBreakEvent);
        if (blockBreakEvent.isCancelled()) {
            new Chat(Lang.CANNOT_PUT_SIGN_HERE).send(sender);
            return true;
        }

        Logger.debug("[Undo Command] Calling SignChangeEvent.");
        SignChangeEvent signChangeEvent = new SignChangeEvent(block, player, lastEntry.getLines());
        Bukkit.getServer().getPluginManager().callEvent(signChangeEvent);
        if (signChangeEvent.isCancelled()) {
            new Chat(Lang.CANNOT_EDIT_SIGN_HERE).send(sender);
            return true;
        }

        Sign sign = (Sign) block.getState();
        String[] lines = signChangeEvent.getLines();
        for (int i = 0; i < 4; i++) {
            sign.setLine(i, player.hasPermission("sign.color") ? ChatColor.translateAlternateColorCodes('&', lines[i]) : lines[i]);
        }
        sign.update();

        Logger.debug("[Undo Command] Player has undone a sign edit.");
        new Chat(Lang.SIGN_UNDONE).send(sender);
        return true;
    }
}
