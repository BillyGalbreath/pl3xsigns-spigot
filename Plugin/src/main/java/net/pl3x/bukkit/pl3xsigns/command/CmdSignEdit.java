package net.pl3x.bukkit.pl3xsigns.command;

import net.pl3x.bukkit.pl3xsigns.Chat;
import net.pl3x.bukkit.pl3xsigns.HistoryEntry;
import net.pl3x.bukkit.pl3xsigns.Logger;
import net.pl3x.bukkit.pl3xsigns.Pl3xSigns;
import net.pl3x.bukkit.pl3xsigns.configuration.Config;
import net.pl3x.bukkit.pl3xsigns.configuration.Lang;
import net.pl3x.bukkit.pl3xsigns.manager.HistoryManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;

import java.util.List;
import java.util.Set;

public class CmdSignEdit implements TabExecutor {
    private final Pl3xSigns plugin;

    public CmdSignEdit(Pl3xSigns plugin) {
        this.plugin = plugin;
    }

    static String join(String[] args) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < args.length; i++) {
            if (i != 1) {
                sb.append(" ");
            }
            sb.append(args[i]);
        }
        return sb.toString();
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            new Chat(Lang.PLAYER_COMMAND).send(sender);
            return true;
        }
        Player player = (Player) sender;
        if (!player.hasPermission("command.signedit")) {
            new Chat(Lang.COMMAND_NO_PERMISSION).send(sender);
            return true;
        }
        if (args.length < 1) {
            new Chat(Lang.NO_LINE_SPECIFIED).send(sender);
            return true;
        }

        Block block;
        if (Config.USE_OLD_LINE_OF_SIGHT.getBoolean()) {
            block = player.getTargetBlock((Set<Material>) null, 5);
        } else {
            block = plugin.getLineOfSightHandler().getTargetBlock(player,
                    Config.LINE_OF_SIGHT_DISTANCE.getInt(),
                    Config.LINE_OF_SIGHT_ACCURACY.getDouble());
        }

        if (block == null || !(block.getState() instanceof Sign)) {
            new Chat(Lang.NOT_LOOKING_AT_SIGN).send(sender);
            return true;
        }
        Integer line;
        try {
            line = Integer.parseInt(args[0]);
            line--;
        } catch (NumberFormatException e) {
            new Chat(Lang.LINE_MUST_BE_NUMBER).send(sender);
            return true;
        }
        if ((line < 0) || (line > 3)) {
            new Chat(Lang.LINE_OUT_OF_RANGE).send(sender);
            return true;
        }

        Sign sign = (Sign) block.getState();
        String[] lines = new String[4];
        for (int i = 0; i < 4; i++) {
            lines[i] = ChatColor.stripColor(sign.getLine(i).replace("\u00a7", "&"));
        }
        lines[line] = (args.length < 2) ? "" : join(args);
        HistoryEntry history = new HistoryEntry(sign); // get current lines of sign

        // call SignChangeEvent (makes the command compatible with protection plugins)
        Logger.debug("[Edit Command] Calling BlockBreakEvent.");
        BlockBreakEvent blockBreakEvent = new BlockBreakEvent(block, player);
        Bukkit.getServer().getPluginManager().callEvent(blockBreakEvent);
        if (blockBreakEvent.isCancelled()) {
            new Chat(Lang.CANNOT_PUT_SIGN_HERE).send(sender);
            return true;
        }

        Logger.debug("[Edit Command] Calling SignChangeEvent.");
        SignChangeEvent signChangeEvent = new SignChangeEvent(block, player, lines);
        Bukkit.getServer().getPluginManager().callEvent(signChangeEvent);
        if (signChangeEvent.isCancelled()) {
            new Chat(Lang.CANNOT_EDIT_SIGN_HERE).send(sender);
            return true;
        }

        lines = signChangeEvent.getLines();
        for (int i = 0; i < 4; i++) {
            sign.setLine(i, player.hasPermission("sign.color") ? ChatColor.translateAlternateColorCodes('&', lines[i]) : lines[i]);
        }
        sign.update();

        Logger.debug("[Edit Command] Sign text edited.");
        HistoryManager.getHistory(player).add(history); // store original lines of sign
        new Chat(Lang.SIGN_EDITED).send(sender);
        return true;
    }
}
